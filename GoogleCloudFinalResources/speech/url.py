from pytube import YouTube
import sys
import subprocess
import os
# https://python-pytube.readthedocs.io/en/latest/
# https://github.com/nficano/pytube/issues/434 -- to fix PyTubes (not ported) issues
if(len(sys.argv) < 1):
    print("No URL entered")
    sys.exit()
else:
    try:
        #YouTube(sys.argv[1]).streams.filter(only_audio=True).first().download()
        results = [each for each in os.listdir("./") if each.endswith('.mp4')]
        result = results[0].replace(")", "")
        result = result.replace("(", "")
        print(result)
        string = "python3 convert.py \"" + result + "\""
        print(string)
        mvCmd = "mv \"" + results[0] + "\" \""  + result + "\""
        print(mvCmd)
        subprocess.call([mvCmd], shell=True)
        subprocess.call([string], shell=True)
        #subprocess.call([newString], shell=True)
    except Exception as ex:
        print(ex)
        print("Exception occured")

