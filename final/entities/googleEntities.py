import six
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types

def getEntities(text):
    #text = 'President Kennedy spoke at the White House.'
    # we need to make this into a string as getEntities has a size minimum (i.e., "I love you" doesn't work")
    newText = ""
    for i in text:
        newText += i + ". "
    client = language.LanguageServiceClient()

    if isinstance(text, six.binary_type):
        text = text.decode('utf-8')

    # Instantiates a plain text document.
    document = types.Document(
        content=newText,
        type=enums.Document.Type.PLAIN_TEXT)

    # Detects entities in the document. You can also analyze HTML with:
    #   document.type == enums.Document.Type.HTML
    entities = client.analyze_entities(document).entities

    '''
    for entity in entities:
        entity_type = enums.Entity.Type(entity.type)
        print('=' * 20)
        print(u'{:<16}: {}'.format('name', entity.name))
        print(u'{:<16}: {}'.format('type', entity_type.name))
        print(u'{:<16}: {}'.format('salience', entity.salience))
        print(u'{:<16}: {}'.format('wikipedia_url',
              entity.metadata.get('wikipedia_url', '-')))
        print(u'{:<16}: {}'.format('mid', entity.metadata.get('mid', '-')))
    '''
    dicti = {}
    for entity in entities:
        entity_type = enums.Entity.Type(entity.type)
        dicti[entity.metadata.get('mid', '-')] = {"name":entity.name, "noun":entity_type.name,
            "salience":entity.salience, "wikipedia_url": entity.metadata.get('wikipedia_url', '-'),
            "mid":entity.metadata.get('mid', '-')}
    #for key, value in dicti.items():
    #    print(key, value)
    return dicti;
