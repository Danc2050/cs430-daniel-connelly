# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .Model import Model
from datetime import datetime
from google.cloud import datastore

class model(Model):
    def __init__(self):
        self.client = datastore.Client('cs410c-son-vu')

    def select(self):
        datadict = {}
        query = self.client.query(kind = 'Recipes')
        entities = query.fetch()
        for row in entities:
            datadict[row['title']] = {'title':row['title'], 'author': row['author'], 'ingredient':row['ingredient'], 'time': row['time'], 'skill': row['skill'], 'description': row['description']}
        return datadict 

    def insert(self, title, author, ingredient, time, skill, description):
        key = self.client.key('Recipes')
        rev = datastore.Entity(key)
        rev.update( {
            'title': title,
            'author' : author,
            'ingredient' : ingredient,
            'time' : time,
            'skill' : skill,
            'description' : description
            })
        self.client.put(rev)
        return True
