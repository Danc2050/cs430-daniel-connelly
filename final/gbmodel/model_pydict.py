"""
Python dictionary model
"""
from .Model import Model

class model(Model):
    def __init__(self):
        self.entities= {}

    def select(self):
        """
        Returns recipes of nested dictionaries
        Each dict in guestentries contains: title, author, ingredient list, time, skill level, and description
        :return: nested dictionaries
        """
        return self.entities

    def insert(self, entities, foreignText, translatedText):
        """
        Appends a new dict of values representing new recipe into recipes
        :param entities: String
        :param foreignText: list
        :return: True
        """


        for entity in entities:
            params = {
                'foreignText': foreignText,
                'translatedText': translatedText,
                'name': entities[entity]["name"],
                'noun': entities[entity]["noun"],
                'salience': entities[entity]["salience"],
                'wikipedia_url': entities[entity]["wikipedia_url"],
                'mid': entities[entity]["mid"]
              }
            self.entities[params['mid']] = params
        #self.recipes[params['title']] = params
        return True
