from .Model import Model
import sqlite3
DB_FILE = 'recipes.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from recipes")
        except sqlite3.OperationalError:
            cursor.execute("create table recipes (title text, author text, ingredient text, time text, skill text, description text)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: title, author, ingredient, time, skill description 
        :return: nested dicts containing all rows of database
        """
        datadict = {}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM recipes")
        dataList = cursor.fetchall()
        for row in dataList:
            datadict[row[0]] = {'title':row[0], 'author': row[1], 'ingredient':row[2], 'time': row[3], 'skill': row[4], 'description': row[5]}

        return datadict
        

    def insert(self, title, author, ingredient, time, skill, description):
        """
        Inserts recipe into database
        :param title: String
        :param author: String
        :param ingredient: String
        :param time: String
        :param skill: String
        :param description: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'title':title, 'author':author, 'ingredient':ingredient, 'time':time, 'skill': skill, 'description': description}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into recipes (title, author, ingredient, time, skill, description) VALUES (:title, :author, :ingredient, :time, :skill, :description)", params)

        connection.commit()
        cursor.close()
        return True
