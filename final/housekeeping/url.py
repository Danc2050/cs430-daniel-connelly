#from pytube import YouTube
import sys
import os
import subprocess
# https://python-pytube.readthedocs.io/en/latest/
# https://github.com/nficano/pytube/issues/434 -- to fix PyTubes (not ported) issues
def download(url):
    try:
#        YouTube(url).streams.filter(only_audio=True).first().download()
        # download
        downloadString = 'youtube-dl --extract-audio \"' + url + "\""
        subprocess.call([downloadString], shell=True)
        results = [each for each in os.listdir("./") if each.endswith('.opus')]
        result = results[0].replace(")", "")
        result = result.replace("(", "")
        print(result)
        string = "python3 housekeeping/convert.py \"" + result + "\""
        print(string)
        # move (get rid of '(' and ')')
        mvCmd = "mv \"" + results[0] + "\" \""  + result + "\""
        print(mvCmd)
        subprocess.call([mvCmd], shell=True)
        # convert
        subprocess.call([string], shell=True)
        return result
    except Exception as ex:
        print(ex)
        print("Exception occured")
#download(sys.argv[1])
'''
if(len(sys.argv) < 1):
    print("No URL entered")
    sys.exit()
else:
'''
