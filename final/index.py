from flask import render_template, redirect, request, url_for
from flask.views import MethodView
import gbmodel
from google.cloud import storage
from housekeeping.url import download
from entities.googleEntities import getEntities
from text.speech2text import transcribe_gcs
from text.translate import translate_text
import subprocess
#import pytube
import os

class Index(MethodView):
    def post(self):
        # Pytube download
        url = request.form['url']
        #download and ffmpeg conversion
        nameOfFile = download(url)
        # upload to bucket
        subprocess.call(['gsutil cp audio.flac gs://cs410c-son-vu/'], shell=True)
        # speech2text API
        gcs_uri = "gs://cs410c-son-vu/audio.flac"
        foreignText = transcribe_gcs(gcs_uri) # returns a list
        # text2speech API
        foreignText, translatedText = translate_text("en", foreignText)
        # entities API
        entities = getEntities(translatedText)
        model = gbmodel.get_model()
        model.insert(entities, foreignText, translatedText)
        os.remove(nameOfFile)
        return redirect(url_for('sign'))


    def get(self):
        model = gbmodel.get_model()
        entities = model.select()
        return render_template('index.html', entities=entities)
