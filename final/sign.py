from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel

class Sign(MethodView):
    def get(self):
        model = gbmodel.get_model()
        entities = model.select()
        return render_template('sign.html', entities=entities)

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.
        """
        return redirect(url_for('index'))
