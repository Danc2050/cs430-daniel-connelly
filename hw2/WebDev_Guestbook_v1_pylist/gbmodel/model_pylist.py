"""
Data is stored in a Python list.  Returns a list of lists
  upon retrieval
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.guestentries = {}

    def select(self):
        """
        Returns guestentries list of lists
        Each list in guestentries contains: name, email, date, message
        :return: List of lists
        """
        return self.guestentries

    def insert(self, Title, Author, Ingredient_list, Time, Skill_level, Description):
        """
        Appends a new list of values representing new message into guestentries
        :param name: String
        :param email: String
        :param message: String
        :return: True
        """
        self.guestentries[Title] = [Title, Author, Ingredient_list, Time, Skill_level, Description]
        return True

