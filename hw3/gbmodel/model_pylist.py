"""
Data is stored in a Python list.  Returns a list of lists
  upon retrieval
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.guestentries = {}

    def select(self):
        """
        Returns guestentries list of lists
        Each list in guestentries contains: Title, Author, Ingredient_list, Time, Skill_level, Description):
        :return: dictionary of lists
        """
        return self.guestentries

    def insert(self, Title, Author, Ingredient_list, Time, Skill_level, Description):
        """
        Appends a new list of values representing new message into guestentries
        Inserts entry into database
        :param Title: String
        :param Author: String
        :param Ingredient_list: String
        :param Time: String
        :param Skill_level: String
        :param Description: String
        :return: True
        """
        self.guestentries[Title] = [Title, Author, Ingredient_list, Time, Skill_level, Description]
        return True

