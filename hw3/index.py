from flask import render_template
from flask.views import MethodView
import gbmodel
#from model_pylist import model

class Index(MethodView):
    """
    List guestbook
    """
    def get(self):
        model1 = gbmodel.get_model()
        entries = [dict(Title=row[0], author=row[1], ingredient_list=row[2], Time=row[3], skill_level=row[4], description=row[5]) for value, row in model1.select().items()]

        return render_template('index.html', entries=entries)
