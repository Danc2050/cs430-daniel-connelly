To run code

virtualenv -p python3 env
source env/bin/activate
pip install -r requirements.txt

docker run -di -p 8080:5000 danc2/hw4large

docker run -di -p 8080:5000 danc2/hw4tiny
