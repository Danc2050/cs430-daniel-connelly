from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'

class model(Model):
    def __init__(self):
        # Make sure our database exists
        self.guestentries = {} # our dictionary that we use to transfer our list of lists to a dictionary (as we had before)
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from recipebook")
        except sqlite3.OperationalError:
            cursor.execute("create table recipebook (Title text, Author text, Ingredient_list, Time, Skill_level, Description)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database ( a list of list )
        Each row contains a list of lists (tuples to be precise) with the following content:
         - Title, Author, Ingredient_list, Time, Skill_level, Description
        The for loop reconverts the list of tuples to a dictionary of lists as before. This way
        we don't need to change any other code in our index.py file.
        :return: Dictionary of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM recipebook")
        list_of_rows = cursor.fetchall()
        for i in range(0, len(list_of_rows)):
            self.guestentries[list_of_rows[i][0]] = [list_of_rows[i][0],list_of_rows[i][1],list_of_rows[i][2],list_of_rows[i][3],list_of_rows[i][4],list_of_rows[i][5]] 
        return self.guestentries

## TODO -- my problem lies inthe inserting, not necessarily the retrieving
    def insert(self, Title, Author, Ingredient_list, Time, Skill_level, Description):
        """
        Inserts entry into database
        :param Title: String
        :param Author: String
        :param Ingredient_list: String
        :param Time: String
        :param Skill_level: String
        :param Description: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'Title':Title, 'Author':Author, 'Ingredient_list':Ingredient_list, 'Time':Time, 'Skill_level':Skill_level, 'Description': Description}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into recipebook (Title, Author, Ingredient_list, Time, Skill_level, Description) VALUES (:Title, :Author, :Ingredient_list, :Time, :Skill_level, :Description)", params)
        connection.commit()
        cursor.close()
        return True
