"""
A simple guestbook flask app.
Daniel Connelly CS 410/510 Internet and Cloud
"""
from flask import Flask
from flask.views import MethodView
from index import Index
from sign import Sign

app = Flask(__name__)       # our Flask app

app.add_url_rule('/',
        view_func=Index.as_view('index'),
        methods=["GET"])

app.add_url_rule('/sign/',
        view_func=Sign.as_view('sign'),
        methods=['GET', 'POST'])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)

