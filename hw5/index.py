from flask import render_template
from flask.views import MethodView
import gbmodel
#from model_pylist import model

class Index(MethodView):
    """
    List guestbook
    """
    def get(self):
        model = gbmodel.get_model()
        #entries = [dict(name=row[0], email=row[1], signed_on=row[2], message=row[3] ) for row in model.select()]
        entries = [dict(Title=row[0], Author=row[1], Ingredient_list=row[2], Time=row[3], Skill_level=row[4], Description=row[5]) for row in model.select()]
        return render_template('index.html', entries=entries)
        #return render_template('index.html', entries=model.select())
